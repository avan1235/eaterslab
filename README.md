# EatersLab

EatersLab is a system monitoring live occupation of cafeterias. System of cameras counts visitors entering and leaving 
lunchroom space. Central endpoint aggregates occupancy reports for owners and customers using mobile or web clients which also serve as information/feedback loop.

The project webpage is available on [https://eaterslab.herokuapp.com/](https://eaterslab.herokuapp.com/). There the 
client app can be downloaded as well as the [API endpoint documentation](https://eaterslab.herokuapp.com/doc) can be tested.
The more useful form of API specification can be downloaded from [here](https://eaterslab.herokuapp.com/api.yaml) as the `yaml` file.

## Documentation

1. [Project pitch](specs/pitch.md)
2. [Requirements specification](specs/requirements.md)
3. [Use cases list](specs/use-cases.md)
4. [System architecture specification](specs/architecture.md)
5. [Testing specification](specs/testing.md)
6. [Project plan](specs/project-plan.md)
7. [Project presentation](https://pooh-labs.github.io/eaterslab/index_en.html)

## Original project

The main project repository can be found [here](https://github.com/pooh-labs/eaterslab).
There you can find all the statistics and history data from project development process.

## Authors

* Krzysztof Antoniak
* Robert Michna
* Maciej Procyk
* Jakub Walendowski
